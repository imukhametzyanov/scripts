import re
import matplotlib.pyplot as plt
import numpy.fft as fft
import numpy as np

class Message:
    def __init__(self, value, modem, base_station, power):
        self.value = value
        self.modem = modem
        self.base_station = base_station
        self.power = power
    def __str__(self):
        return "{0} {1}".format(self.value, self.modem)
    def __repr__(self):
        return str(self)
messages = []
if __name__ == "__main__":
    f = open('data.txt', 'r')
    full_line = ""
    line_counter = 0
    for line in f:
        #print (line)
        full_line = full_line + line
        line_counter = line_counter + 1
        if line_counter == 3:
            line_counter = 0 
            parts_of_message = re.findall(r'\w+', full_line)
            #print(parts_of_message)
            messages.append(
                Message(parts_of_message[2], parts_of_message[1], parts_of_message[0], int(parts_of_message[16])))
            full_line = '';
    f.close()
    print ("Removing duplicates")
    find_duplicates = True
    while find_duplicates:
        find_duplicates = False
        for i in range(len(messages) - 1):
            if messages[i].value == messages[i+1].value and messages[i].modem == messages[i+1].modem:
                print(messages[i])
                del messages[i]
                find_duplicates = True
                break


    messages.reverse()
    print ("Received messages num = {0}".format(len(messages)))
##    print ("Messages:")
##    for mes in messages:
##        print(mes)


    
            
    power_sum = 0
    for mes in messages:
        power_sum = power_sum + mes.power
    
    print("Average power = {0}".format(power_sum / len(messages) ))

    lost_iterators = []

    max_iterator = 0
    
    iterator = -1
    for mes in messages:
        iterator = iterator + 1
        if max_iterator < iterator:
            max_iterator = iterator
        it = mes.value[0:4]
        it = int(it, 16)
        if it != iterator:
            for i in range(iterator, it):
                lost_iterators.append(i)
            if (it - iterator) > 1:
                print("{0}-{1}, len = {2}".format( '%02x' % iterator, '%02x' % (it - 1), it - iterator))
            elif (it - iterator) == 1:
                print('%02x' % iterator)
            else:
                print("WTF!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                raise Exception("WTF!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            iterator = it

    print("lost_iterators")
    print(lost_iterators)


    MESSAGE_COUNT_IN_SEQUENCE = 8

    power_in_time = [0 for x in range(max_iterator//MESSAGE_COUNT_IN_SEQUENCE + 1)]
    for mes in messages:
        it = mes.value[0:4]
        it = int(it, 16)
        power_in_time[it//MESSAGE_COUNT_IN_SEQUENCE] += mes.power
    for i in range(len(power_in_time)):
        power_in_time[i] = power_in_time[i]/MESSAGE_COUNT_IN_SEQUENCE
    print("power_in_time")
    print(power_in_time)
    
    lost_in_time = [0 for x in range(max_iterator//MESSAGE_COUNT_IN_SEQUENCE + 1)]
    for it in lost_iterators:
        lost_in_time[it//MESSAGE_COUNT_IN_SEQUENCE] += 1
    print("lost_in_time")
    print(lost_in_time)

##    f = open('output.csv', 'w')
##    for val in lost_in_time:
##        f.write(str(val) + '\n')
##    f.close()

    FREQUENCY_STEP = 24.8

    for i in range(len(lost_in_time)):
        lost_in_time[i] = lost_in_time[i]/MESSAGE_COUNT_IN_SEQUENCE
    x_axis = [i*FREQUENCY_STEP for i in range(max_iterator//MESSAGE_COUNT_IN_SEQUENCE + 1)]
    plt.plot(x_axis, lost_in_time)
    plt.ylabel('Errors')
    plt.xlabel('Frequency offset')
    plt.show()

    plt.plot(x_axis, power_in_time)
    plt.ylabel('Power')
    plt.xlabel('Frequency offset')
    plt.show()

##    average = sum(lost_in_time)/len(lost_in_time)
##    for i in range(len(lost_in_time)):
##        lost_in_time[i] = lost_in_time[i] - average
##    sp = np.fft.rfft(lost_in_time)
##    freq = np.fft.rfftfreq(len(lost_in_time))
##    plt.plot(freq, np.absolute(sp))
##    plt.show()

    
##    average = sum(lost_in_time)/len(lost_in_time)
##    for i in range(len(lost_in_time)):
##        lost_in_time[i] = lost_in_time[i] - average
##    sp = np.fft.fft(lost_in_time)
##    plt.plot( sp.real)
##    plt.show()


    powers = [mes.power for mes in messages]
    plt.hist(powers)
    plt.xlabel("Value")
    plt.ylabel("Frequency")
    plt.show()
