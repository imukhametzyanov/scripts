#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy.fft as fft
import numpy as np



SamplesDumpPath = "samples.bin"
GRAPHIC_SIZE = 12500

samples = 0
r_sample = 0
magnitudes = []

if __name__ == '__main__':
    idata = [0 for i in range(GRAPHIC_SIZE)]
    qdata = [0 for i in range(GRAPHIC_SIZE)]
    print("Open file")
    fileDesc = open(SamplesDumpPath, "rb")
    t = np.arange(GRAPHIC_SIZE)
    freq = np.fft.fftfreq(t.shape[-1])
    
    while True:
##        i = np.fromfile(fileDesc, dtype=np.int16)
##        q = np.fromfile(fileDesc, dtype=np.int16)
##        idata[samples] = i
##        idata[samples] = q
        data = fileDesc.read(4*GRAPHIC_SIZE)
       ## print(data)
        for i in range(GRAPHIC_SIZE):
            start = i * 4
            idata[samples] = int.from_bytes(data[start:(start+2)], byteorder='little', signed=True)
            qdata[samples] = int.from_bytes(data[(start+2):(start+4)], byteorder='little', signed=True)
            samples += 1
        r_sample += GRAPHIC_SIZE      
        samples = 0  

        #module_data = [math.sqrt(idata[i]*idata[i] + qdata[i]*qdata[i]) for i in range(GRAPHIC_SIZE)]

        complex_data = [complex(idata[i], qdata[i]) for i in range(GRAPHIC_SIZE)]
        sp = np.fft.fft(complex_data)
        

        magnitude = 20 * np.log10(np.abs(sp))

        
        
        magnitudes.append(max(magnitude))
        
        if len(magnitudes)%(2560*7) == 0:
            fig = plt.figure()

            ax1 = fig.add_subplot(311)
            ax1.plot(range(r_sample - GRAPHIC_SIZE, r_sample), idata)
            ax1.plot(range(r_sample - GRAPHIC_SIZE, r_sample),qdata)
            
            #ax2 = fig.add_subplot(412)
            #ax2.plot(range(r_sample - GRAPHIC_SIZE, r_sample),module_data)

            ax3 = fig.add_subplot(312)
            #ax3.axis([-0.05,0.05,47,80])
            #ax3.axis([-0.1,0.1,30,80])
            ax3.plot(freq, magnitude)

            ax4 = fig.add_subplot(313)
            ax4.plot( magnitudes)
            plt.show()

        
        
        

            

    fileDesc.close()






##
###!/usr/bin/env python
##import os
##import re
##import matplotlib.pyplot as plt
##import numpy.fft as fft
##import numpy as np
##import math


##
##SamplesDumpPath = "samples.bin"
##GRAPHIC_SIZE = 256
##
##samples = 0
##r_sample = 0
##magnitudes = []
##
##if __name__ == '__main__':
##    t = np.arange(2560000)
##    iidata = np.sin(0.1*t)
##    qqdata = -np.cos(0.1*t)
##    
##    idata = [0 for i in range(GRAPHIC_SIZE)]
##    qdata = [0 for i in range(GRAPHIC_SIZE)]
##
##    while True:
####        i = np.fromfile(fileDesc, dtype=np.int16)
####        q = np.fromfile(fileDesc, dtype=np.int16)
####        idata[samples] = i
####        idata[samples] = q
## 
##       ## print(data)
##        value = iidata[r_sample]
##        idata[samples] = value
##        value = qqdata[r_sample]
##        qdata[samples] = value
##        r_sample += 1
##        
##        samples += 1
##        if samples == GRAPHIC_SIZE:
##  
##            t = np.arange(GRAPHIC_SIZE)
##
##            module_data = [math.sqrt(idata[i]*idata[i] + qdata[i]*qdata[i]) for i in range(GRAPHIC_SIZE)]
##
##            complex_data = [complex(idata[i], -qdata[i]) for i in range(GRAPHIC_SIZE)]
##            sp = np.fft.fft(complex_data)
##            freq = np.fft.fftfreq(t.shape[-1])
##
##            magnitude = 10 * np.log10(np.abs(sp))
##            
##            magnitudes.append(max(magnitude))
##            
##            if len(magnitudes)%1 == 0:
##                fig = plt.figure()
##
##                ax1 = fig.add_subplot(411)
##                ax1.plot(range(r_sample - GRAPHIC_SIZE, r_sample), idata)
##                ax1.plot(range(r_sample - GRAPHIC_SIZE, r_sample),qdata)
##                
##                ax2 = fig.add_subplot(412)
##                ax2.plot(range(r_sample - GRAPHIC_SIZE, r_sample),module_data)
##
##                ax3 = fig.add_subplot(413)
##                #ax3.axis([-0.05,0.05,47,80])
##                #ax3.axis([-0.1,0.1,30,80])
##                ax3.plot(freq, magnitude)
##
##                ax4 = fig.add_subplot(414)
##                ax4.plot( magnitudes)
##                plt.show()
##
##            
##            
##            samples = 0











