#!/usr/bin/env python
import os
import re
import matplotlib.pyplot as plt
import numpy.fft as fft
import numpy as np
import math



SamplesDumpPath = "data"
GRAPHIC_SIZE = 70 #99


magnitudes = []
light_diff = []
result_temp = []
result = []

if __name__ == '__main__':
    
    print("Open file")
    fileDesc = open(SamplesDumpPath, "rb")
    

    data = fileDesc.read(2)
    value = int.from_bytes(data, byteorder='little', signed=False)

    print("Average light was {0}".format(value))

    for i in range(GRAPHIC_SIZE):
        data = fileDesc.read(2)
        value = int.from_bytes(data, byteorder='little', signed=False)
        light = value & 0x00ff
        light = light * 0.2
        light_diff.append(light)

        value = value >> 8
        mag = value & 0x003f
        magnitudes.append(mag)

        value = value >> 6
        res = value & 0x0001
        result_temp.append(res)

        value = value >> 1
        res = value & 0x0001
        result.append(res)

    fig = plt.figure()

    ax1 = fig.add_subplot(411)
    ax1.plot( light_diff, linestyle='--', marker='o')
    ax1.set_title("Darkening, L_aver/L")
    
    ax2 = fig.add_subplot(412)
    ax2.plot( magnitudes, linestyle='--', marker='o')
    ax2.set_title("Magnitude")

    ax3 = fig.add_subplot(413)
    ax3.plot( result_temp, linestyle='--', marker='o')
    ax3.set_title("Instant result")

    ax4 = fig.add_subplot(414)
    ax4.plot( result, linestyle='--', marker='o')
    ax4.set_title("Finite result")
    
    plt.show()

            

    fileDesc.close()













