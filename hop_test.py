import re
import matplotlib.pyplot as plt
import numpy.fft as fft
import numpy as np
import math


CHANNELS_COUNT = 100
setFreq = 0



def UNB_get_channel(serial, SetFreq, counter):
    SetFreq = SetFreq
    freq_increment = serial%(CHANNELS_COUNT-10)#freq_increment = 0..CHANNELS_COUNT-11
    freq_increment += 5#freq_increment = 5..CHANNELS_COUNT-6

    if counter%16 == 0:
        freq_increment += (serial//(CHANNELS_COUNT-10))%5 - 2
    
    SetFreq += freq_increment
    SetFreq = SetFreq%(CHANNELS_COUNT-1)
    return SetFreq

##def UNB_get_channel(serial, SetFreq):
##    SetFreq = SetFreq
##    freq_increment = serial%(CHANNELS_COUNT-3)
##    freq_increment += 1
##    SetFreq += freq_increment
##    SetFreq = SetFreq%(CHANNELS_COUNT-1)
##    return SetFreq

##def UNB_get_channel(serial, SetFreq):
##    SetFreq = SetFreq
##    freq_increment = serial%(CHANNELS_COUNT-1)
##    SetFreq += freq_increment
##    SetFreq = SetFreq%(CHANNELS_COUNT-1)
##    return SetFreq



if __name__ == "__main__":
##    f = open('data.txt', 'r')
##    full_line = ""
##    line_counter = 0
##    for line in f:
##        #print (line)
##        full_line = full_line + line
##        line_counter = line_counter + 1
##        if line_counter == 3:
##            line_counter = 0
##            ##print(full_line)
##            parts_of_message = re.findall(r'\w+', full_line)
##            ##print(parts_of_message)
##            if(len(parts_of_message) == 26):
##                messages.append(
##                    Message(parts_of_message[10], parts_of_message[1], parts_of_message[0], int(parts_of_message[24])))
##            full_line = '';
##    f.close()
##    print ("Removing duplicates")
##    find_duplicates = True
##    while find_duplicates:
##        find_duplicates = False
##        for i in range(len(messages) - 1):
##            if messages[i].value == messages[i+1].value and messages[i].modem == messages[i+1].modem:
##                print(messages[i])
##                del messages[i]
##                find_duplicates = True
##                break
##
##
##    messages.reverse()
####    print(messages)
##    print ("Received messages num = {0}".format(len(messages)))
##
##    parkings = [Parking(mes) for mes in messages if len(mes.value)>=12]
##
##    print ("parkings")
##    for park in parkings:
##        print(park)
##    print ("On parkings")
##    for park in parkings:
##        if park.state == '11':
##            print(park)
##
##    max_val = [min(max(p.x_magnit, p.y_magnit, p.z_magnit),50) for p in parkings if p.state == '11']
##    print(max_val)
##    full_val = [min(p.full_magnit, 50) for p in parkings if p.state == '11']
##    print(full_val)

    channel_hist = [0 for i in range(CHANNELS_COUNT-1)]

    for serial in range(2000):
        print(serial)
        setFreq = 0
        for it in range(10000):
            setFreq = UNB_get_channel(serial, setFreq, it)
            channel_hist[setFreq] += 1

    plt.plot(range(CHANNELS_COUNT-1), channel_hist)
    plt.xlabel("Channel")
    plt.ylabel("Frequency")
    plt.show()



