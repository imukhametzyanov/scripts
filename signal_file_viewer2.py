#!/usr/bin/env python
import os
import re
import matplotlib.pyplot as plt
import numpy.fft as fft
import numpy as np
import math


error_num = 0
SamplesDumpPath = "samples.bin"
GRAPHIC_SIZE = 1000000

samples = 0
r_sample = 0
magnitudes = []

if __name__ == '__main__':
    idata = [0 for i in range(GRAPHIC_SIZE)]
    qdata = [0 for i in range(GRAPHIC_SIZE)]
    print("Open file")
    fileDesc = open(SamplesDumpPath, "rb")

    while True:
##        i = np.fromfile(fileDesc, dtype=np.int16)
##        q = np.fromfile(fileDesc, dtype=np.int16)
##        idata[samples] = i
##        idata[samples] = q
        data = fileDesc.read(2)
       ## print(data)
        value = int.from_bytes(data, byteorder='little', signed=True)
        idata[samples] = value
        data = fileDesc.read(2)
        value = int.from_bytes(data, byteorder='little', signed=True)
        qdata[samples] = value
        r_sample += 1
        
        samples += 1
        if samples == GRAPHIC_SIZE:
  
            t = np.arange(GRAPHIC_SIZE)

            module_data = [idata[i] - qdata[i] for i in range(GRAPHIC_SIZE)]
            for d in module_data:
                if d != 0:
                    error_num += 1


            complex_data = [complex(idata[i], qdata[i]) for i in range(GRAPHIC_SIZE)]
            sp = np.fft.fft(complex_data)
            freq = np.fft.fftfreq(t.shape[-1])

            magnitude = 10 * np.log10(np.abs(sp))
            
            magnitudes.append(max(magnitude))
            
            if len(magnitudes) == 10:
                fig = plt.figure()

                ax1 = fig.add_subplot(411)
                ax1.plot(range(r_sample - GRAPHIC_SIZE, r_sample), idata)
                ax1.plot(range(r_sample - GRAPHIC_SIZE, r_sample),qdata)
                
                ax2 = fig.add_subplot(412)
                ax2.plot(range(r_sample - GRAPHIC_SIZE, r_sample),module_data)

                #ax3 = fig.add_subplot(413)
                #ax3.axis([-0.05,0.05,47,80])
                #ax3.axis([-0.1,0.1,30,80])
                #ax3.plot(freq, magnitude)

                #ax4 = fig.add_subplot(414)
                #ax4.plot( magnitudes)
                print ( "Error_num = {0}".format(error_num))
                plt.show()

            
            
            samples = 0

            

    fileDesc.close()






##
###!/usr/bin/env python
##import os
##import re
##import matplotlib.pyplot as plt
##import numpy.fft as fft
##import numpy as np
##import math


##
##SamplesDumpPath = "samples.bin"
##GRAPHIC_SIZE = 256
##
##samples = 0
##r_sample = 0
##magnitudes = []
##
##if __name__ == '__main__':
##    t = np.arange(2560000)
##    iidata = np.sin(0.1*t)
##    qqdata = -np.cos(0.1*t)
##    
##    idata = [0 for i in range(GRAPHIC_SIZE)]
##    qdata = [0 for i in range(GRAPHIC_SIZE)]
##
##    while True:
####        i = np.fromfile(fileDesc, dtype=np.int16)
####        q = np.fromfile(fileDesc, dtype=np.int16)
####        idata[samples] = i
####        idata[samples] = q
## 
##       ## print(data)
##        value = iidata[r_sample]
##        idata[samples] = value
##        value = qqdata[r_sample]
##        qdata[samples] = value
##        r_sample += 1
##        
##        samples += 1
##        if samples == GRAPHIC_SIZE:
##  
##            t = np.arange(GRAPHIC_SIZE)
##
##            module_data = [math.sqrt(idata[i]*idata[i] + qdata[i]*qdata[i]) for i in range(GRAPHIC_SIZE)]
##
##            complex_data = [complex(idata[i], -qdata[i]) for i in range(GRAPHIC_SIZE)]
##            sp = np.fft.fft(complex_data)
##            freq = np.fft.fftfreq(t.shape[-1])
##
##            magnitude = 10 * np.log10(np.abs(sp))
##            
##            magnitudes.append(max(magnitude))
##            
##            if len(magnitudes)%1 == 0:
##                fig = plt.figure()
##
##                ax1 = fig.add_subplot(411)
##                ax1.plot(range(r_sample - GRAPHIC_SIZE, r_sample), idata)
##                ax1.plot(range(r_sample - GRAPHIC_SIZE, r_sample),qdata)
##                
##                ax2 = fig.add_subplot(412)
##                ax2.plot(range(r_sample - GRAPHIC_SIZE, r_sample),module_data)
##
##                ax3 = fig.add_subplot(413)
##                #ax3.axis([-0.05,0.05,47,80])
##                #ax3.axis([-0.1,0.1,30,80])
##                ax3.plot(freq, magnitude)
##
##                ax4 = fig.add_subplot(414)
##                ax4.plot( magnitudes)
##                plt.show()
##
##            
##            
##            samples = 0











