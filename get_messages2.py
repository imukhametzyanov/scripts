import re
import matplotlib.pyplot as plt
import numpy.fft as fft
import numpy as np

class Message:
    def __init__(self, value, modem, base_station, power):
        self.value = value
        self.modem = modem
        self.base_station = base_station
        self.power = power
    def __str__(self):
        return "{0} {1}".format(self.value, self.modem)
    def __repr__(self):
        return str(self)
messages = []
if __name__ == "__main__":
    f = open('data.txt', 'r')
    full_line = ""
    line_counter = 0
    for line in f:
        #print (line)
        full_line = full_line + line
        line_counter = line_counter + 1
        if line_counter == 3:
            line_counter = 0 
            parts_of_message = re.findall(r'\w+', full_line)
            #print(parts_of_message)
            messages.append(
                Message(parts_of_message[2], parts_of_message[1], parts_of_message[0], int(parts_of_message[16])))
            full_line = '';
    f.close()
    print ("Removing duplicates")
    find_duplicates = True
    while find_duplicates:
        find_duplicates = False
        for i in range(len(messages) - 1):
            if messages[i].value == messages[i+1].value and messages[i].modem == messages[i+1].modem:
                print(messages[i])
                del messages[i]
                find_duplicates = True
                break


    messages = [ m for m in messages if m.modem == "1EDF0"]
    messages.reverse()


    
    
    print ("Messages:")
    for mes in messages:
        print(mes)

    print("Finding lost iterators")
    lost_iterators = []

    max_iterator = 0
    
    iterator = -1

    sum_power = 0

    count_on_len = { i+2 : 0 for i in range(8)}
    
    for mes in messages:
        sum_power += mes.power;
        if max_iterator < iterator:
            max_iterator = iterator
        it = mes.value[0:4]
        count_on_len[ len(mes.value)//2 ] += 1
        it = int(it, 16)
        if iterator == -1:
            iterator = it
        if it != iterator:
            for i in range(iterator, it):
                lost_iterators.append(i)
            if (it - iterator) > 1:
                print("{0}-{1}, len = {2}".format( '%04x' % iterator, '%04x' % (it - 1), it - iterator))
            elif (it - iterator) == 1:
                print('%04x' % iterator)
            else:
                print("WTF!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                print('%04x' % iterator)
                raise Exception("WTF!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            iterator = it
        iterator = iterator + 1

    

      
    print("lost_iterators")
    print(lost_iterators)

    print ("Received messages num = {0}, lost = {1}".format
                                       (len(messages),len(lost_iterators)))  

    print("average power = {}".format(sum_power/len(messages)))

    print("len hist = {}".format(count_on_len))

    


    
