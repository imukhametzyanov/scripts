import subprocess
import os
import sys

dir_to_open = { "receiver":"C:\\work\\workspace\\receiver_m7",
                "watermeter":"C:\\work\\workspace\\watermeteraxsem",
                "parkingsensor":"C:\\work\\workspace\\parking-sensor",
                "prototests":"C:\\work\\workspace\\proto_tests",
                "carcounter":"C:\\work\\workspace\\_car_counter_repoDEV",
                "devkitrm3":"C:\\work\\workspace\\devkit_rm3_demo",
                "workspace":"C:\\work\\workspace",
                "pdf":"C:\\work\\pdf",
                "scripts":"C:\\work\\workspace\\scripts",
		"vmshare":"C:\\work\\vmwareshare",
                "utils":"C:\\work\\utils"}

projects_to_open ={
    "receiver":
    ["explorer",
     "C:\\work\\workspace\\receiver_m7\\project\\MDK-ARM\\project.uvprojx"],
    "enetbootloaderreceiver":
    ["explorer",
     "C:\\work\\workspace\\receiver_m7\\project\\enet bootloader\\MDK-ARM\\project.uvprojx"],
    "carcounterslave":
    ["explorer",
     "C:\\work\workspace\\_car_counter_repoDEV\\CarCounter\\car_counter.eww"],
    "carcountermaster":
    ["explorer",
     "C:\\work\workspace\\_car_counter_repoDEV\\CarCounterMaster\\car_counter_master.eww"],
    "watermeter":
    ["explorer",
     "C:\\work\\workspace\\watermeteraxsem\\base_project\\MASTER\\MASTER.cbp"],
}

programs_to_run ={
    "ipython":"C:\\Users\\ilnur\\AppData\\Local\\Programs\\Python\\Python35\\Scripts\\ipython.exe",
    "sdrsharp":"C:\\Program Files (x86)\\sdrsharp\\SDRSharp.exe",
    "nwsdrsharp":"C:\\work\\sdr_nwave\\SDRSharp.exe",
    "linuxvmware":["C:\\Program Files (x86)\\VMware\\VMware Player\\vmplayer.exe",
                    "C:\\Users\\ilnur\\Documents\\Virtual Machines\\Linux Mint\\Linux Mint.vmx"]
}

extension_program_dict = {}

dir_to_find_in = ["C:\\work"]
internal_exluded_dirs_to_find = [".git", "EnergyMicro", "emlib", "Debug", "Release"]

if sys.version_info < (3, 0):
    def mir_input(s):
        return raw_input(s)
else:
    def mir_input(s):
        return input(s)

def find_in_dict(things, thing, dict_where):
    print("Available "+things+":")
    for key in sorted(dict_where.keys()):
        print("  "+key)
    
    while True:
        location = mir_input("Enter your "+thing+": ")
        start_match = list()
        for key, value in sorted(dict_where.items()):
            if location == key:
                return value
            elif key.startswith(location):
                start_match.append([key, value])
        startwith_len = len(start_match)
        if startwith_len == 1:
            return start_match[0][1]
        elif startwith_len > 1:
            print("Variants(similar begining):")
            for pair in start_match:
                print("  "+pair[0])


def open_directory():
    subprocess.Popen(["explorer",
                      find_in_dict("locations", "location", dir_to_open)])


def open_project():
    subprocess.Popen(find_in_dict("projects", "project", projects_to_open))

    
def run_program():
    subprocess.Popen(find_in_dict("programs", "program", programs_to_run))


def open_file_by_extension(fullname):
    extension = fullname.split(".")[-1]
    if os.path.isfile(fullname) and extension in extension_program_dict:
        subprocess.Popen([extension_program_dict[extension], fullname])
    else:
        subprocess.Popen(["explorer", fullname])


def find_and_open():
    import re

    while True:
        search_counter = 0
        pattern = mir_input("Enter pattern to match(python re module): ")
        items = set()
        for location in dir_to_find_in:
            for root, dirs, files in os.walk(location, topdown=True):

                dirs[:] = [d for d in dirs if d not in internal_exluded_dirs_to_find]
                for name in files+dirs:
                    fullname = os.path.join(root, name)
                    try:
                        search_counter += 1
                        if re.search(pattern, fullname, flags=re.IGNORECASE) != None:
                            items.add(fullname)
                    except Exception as e:
                        print(e)
                        input("Enter anything to quit")
                        raise
        print("search_counter = ", search_counter)
        items = list(sorted(items))
        if len(items) == 1:
            open_file_by_extension(items[0])
            return
        elif len(items) > 1:
            for index, item in enumerate(items,1):
                print("{0:2}:{1}".format(index, item))
            try:
                indexes = input("Enter indexes or non-digit key to continue: ")
                indexes = indexes.split(" ")

                for x in indexes:
                    x = int(x)

                    if x <= len(items) or x == 0:
                        open_file_by_extension(items[x-1])
                    else:
                        print("Wrong index")
                return
            except ValueError:
                print("continue")
        else:
            print("No such files")


commands = {"open" : open_directory,
            "find" : find_and_open,
            "projectopen" : open_project,
            "run" : run_program}    


def main():
    find_in_dict("commands", "command", commands)()
    
    
if __name__ == "__main__":
    main()

