import re
import matplotlib.pyplot as plt
import numpy.fft as fft
import numpy as np

POWERS_NUM  = 12

class Message:
    def __init__(self, value, modem, base_station, power):
        self.value = value
        self.modem = modem
        self.base_station = base_station
        self.power = power
    def __str__(self):
        return "{0} {1}".format(self.value, self.modem)
    def __repr__(self):
        return str(self)
messages = []
if __name__ == "__main__":
    f = open('data.txt', 'r')
    full_line = ""
    line_counter = 0
    for line in f:
        #print (line)
        full_line = full_line + line
        line_counter = line_counter + 1
        if line_counter == 3:
            line_counter = 0 
            parts_of_message = re.findall(r'\w+', full_line)
            #print(parts_of_message)
            messages.append(
                Message(parts_of_message[2], parts_of_message[1], parts_of_message[0], int(parts_of_message[16])))
            full_line = '';
    f.close()
    print ("Removing duplicates")
    find_duplicates = True
    while find_duplicates:
        find_duplicates = False
        for i in range(len(messages) - 1):
            if messages[i].value == messages[i+1].value and messages[i].modem == messages[i+1].modem:
                print(messages[i])
                del messages[i]
                find_duplicates = True
                break

##    print ("Gen messages:")
##    for mes in messages:
##        if mes.modem != "1DF1A":
##            print(mes)
##
##    messages = [ m for m in messages if m.modem == "1DF1A"]
    messages.reverse()


    
    print ("Received messages num = {0}".format(len(messages)))
##    print ("Messages:")
##    for mes in messages:
##        print(mes)

    lost = [ 0 for i in range(POWERS_NUM)]
    received = [ 0 for i in range(POWERS_NUM)]
    message_range = [ 0 for i in range(POWERS_NUM)]
    
    print("Finding lost iterators")
    for power in range(POWERS_NUM):
        print ("Power = {0}".format(power))
        lost_iterators = []

        max_iterator = -1
        
        iterator = -1

        first_iterator = 0
        for mes in messages:
            mes_power = mes.value[3:4]
            mes_power = int(mes_power, 16)
            if mes_power == power:
                received[power] += 1
                
                it = mes.value[0:3]
                it = int(it, 16)
                if iterator == -1:
                    iterator = it - 1
                    first_iterator = it
                iterator = iterator + 1
                if max_iterator < it:
                    max_iterator = it
                
                if it != iterator:
                    for i in range(iterator, it):
                        lost_iterators.append(i)
                    if (it - iterator) > 1:
                        print("{0}-{1}, len = {2}".format( '%04x' % iterator, '%04x' % (it - 1), it - iterator))
                    elif (it - iterator) == 1:
                        print('%04x' % iterator)
                    else:
                        print("WTF!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                        print('%04x' % iterator)
                        raise Exception("WTF!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                    iterator = it
        message_range[power] = max_iterator - first_iterator + 1
        print("lost_iterators = {0}".format(lost_iterators))
        lost[power] = len(lost_iterators)

    lost_part = [ 0 for i in range(POWERS_NUM)]
    for p in range(POWERS_NUM):
        print("Power = {0}, mes num = {1}, lost = {2}, received = {3}".format(p, message_range[p], lost[p], received[p]))
        if message_range[p]!= 0:
            lost_part[p] = lost[p]/message_range[p]
        else:
            lost_part[p] = 1



    max_mes_num = max(message_range)
    lost_part1 = [ (max_mes_num - received[i])/max_mes_num for i in range(POWERS_NUM)]



    fig = plt.figure()
    
    ax1 = fig.add_subplot(311)
    ax1.plot(range(POWERS_NUM), lost_part)
    ax1.axis([0,POWERS_NUM-1,0,1.1])
    ax1.set_title("naive lost part")
    
    ax2 = fig.add_subplot(312)
    ax2.plot(range(POWERS_NUM), lost_part1)
    ax2.axis([0,POWERS_NUM-1,0,1.1])
    ax2.set_title("lost part")

    ax2 = fig.add_subplot(313)
    ax2.plot(range(POWERS_NUM), received)
    ax2.set_title("received")
    ax2.axis([0,POWERS_NUM-1,0,max_mes_num+1])

    plt.show()
