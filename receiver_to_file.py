#!/usr/bin/env python
import os


from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor

from struct import *

ReceiverIP = '192.168.0.120'
ReceiverPort = 12016



SettingsHeader = 0x01
SampleRate = 0x02
Gain = 80
Frequency = 866670000
CorrectStatus = 0x00
InitStruct = pack('<BBLBB', SettingsHeader, SampleRate, Frequency, Gain, CorrectStatus)

SamplesDumpPath = "samples.bin"


class ParserUDP(DatagramProtocol):
    def startProtocol(self):
        self.transport.connect(ReceiverIP, ReceiverPort);
        self.sendDatagram()

    def sendDatagram(self):
        print "Configuration datagram sent"
        datagram = InitStruct
        self.transport.write(datagram)
    SampleCounter = 0
    fileDesc = None
    def datagramReceived(self, datagram, address):
        answer = self.parseDatagram(datagram)
        if answer is not None:
            self.transport.write(answer, address)

    def parseDatagram(self, datagram):
        if len(datagram) < 1:
            reactor.stop()
            raise Exception("Datagram too short")
        elif datagram[0] == '\x01' and len(datagram) < 100:
            return self.parseSettings(datagram)
        elif len(datagram) > 100:
            self.parseSamples(datagram)
            return None

    def parseSettings(self, datagram):
        settings = dict(zip(['header', 'sample_rate', 'freq', 'gain', 'status']
                            , unpack("<BBLBB", datagram[0:8])))
        if settings['sample_rate'] != SampleRate:
            print 'Settings: samplerate is incorrect: %X'%settings['sample_rate']
        if settings['gain'] != Gain:
            print 'Settings: gain is incorrect: %X'%settings['gain']
        if settings['freq'] != Frequency:
            print 'Settings: frequency is incorrect: %d'%settings['freq']
        if settings['status'] != CorrectStatus:
            reactor.stop()
            raise Exception("Settings: FAIL! (%X)"%settings['status'])

        print "Query for flow..."
        return '\x02\x02'
        

    def parseSamples(self, datagram):
        START =  20000
        LAST =  1220000
        if self.SampleCounter == 0:
            self.fileDesc = open(SamplesDumpPath, "wb")
            print("Open file")
        if self.SampleCounter == START:
            print("Start")
        if self.SampleCounter == LAST:
            self.fileDesc.close()
            print("File is ready")
        
        #print "Datagram saved (ID: %u)"%((int(datagram[1])<<8) | int(datagram[2]))
        d = bytearray(datagram)
        if (self.SampleCounter >= START) and (self.SampleCounter < LAST):
            self.fileDesc.write(datagram[4:1028])
        self.SampleCounter += 1
        counter = d[2] + (d[3] << 8)
        if self.Counter != counter:
            diff = counter - self.Counter
            if diff == 1:
                print str(self.Counter) + " != " + str(counter)
            else:
                print str(self.Counter) + " != " + str(counter) + ', diff = ' + str(diff)
            self.Counter = counter
        self.Counter = self.Counter + 1
        if self.Counter == 0x10000:
            self.Counter = 0
        if self.Counter == 0:
            self.Toggle = self.Toggle + 1
            if self.Toggle == 64:
                self.Toggle = 0
                os.system('cls')  # on windows
            else:
                print "-----------------"
        #if self.Counter % 0x2000 == 0:
        #    self.sendDatagram()
    Counter = 0x00
    Toggle = 0

class ReceiverConfiguratorUDP(DatagramProtocol):
    def startProtocol(self):
        self.transport.connect(ReceiverIP, ReceiverPort);
        self.sendDatagram()

    def sendDatagram(self):
        datagram = InitStruct
        self.transport.write(datagram)

def main():
##    fileDesc = open(SamplesDumpPath, "w")
##    elements = [0, 200, 50, 25, 10, 255]
##    d = bytearray(elements)
##    fileDesc.write(d[2:6])
##    fileDesc.close()
    
    reactor.listenUDP(ReceiverPort, ParserUDP())
    reactor.run()

if __name__ == '__main__':
    main()









