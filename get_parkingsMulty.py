#! python3
import re
import operator
import pprint
import datetime

class Message:
    def __init__(self, value, modem, base_station, power, date_time):
        self.value = value
        self.modem = modem
        self.base_station = base_station
        self.power = power
        self.date_time = date_time
        self.time = datetime.datetime.strptime(date_time, "%y-%m-%d %H:%M:%S")
        self.state = 0 if self.value[3] == '0' else 1
    def __str__(self):
        return "{0} {1} {2}".format(self.value, self.modem, self.time)
    def __repr__(self):
        return str(self)

if __name__ == "__main__":
    messages = []
    pp = pprint.PrettyPrinter(indent=4)
    f = open('data.txt', 'r')

    line_counter = 0
    lines = [None, None, None]
    for line in f:
        #print (line)
        lines[line_counter] = line.split()
        line_counter = line_counter + 1
        if line_counter == 3:
            line_counter = 0
            messages.append(
                Message(lines[1][0], lines[0][1], lines[0][0], None, lines[2][1] + " " + lines[2][2]))
            full_line = '';
    f.close()
    print ("Removing duplicates")
    find_duplicates = True
    while find_duplicates:
        find_duplicates = False
        for i in range(len(messages) - 1):
            if messages[i].value == messages[i+1].value and messages[i].modem == messages[i+1].modem:
                print(messages[i])
                del messages[i]
                find_duplicates = True
                break


    messages.reverse()
    debug_messages = list(filter(lambda x: x.value[2:4] == "DD", messages))
    messages = list(filter(lambda x: x.value[2:4] != "DD", messages))
    
    print ("Received messages num = {0}".format(len(messages)))

    
    print("messages = ")
    pp.pprint(messages)
    print("debug_messages = ")
    pp.pprint(debug_messages)


    print("\n-----------------------\nGroups by time = ")
    sequence = None
    for i in range(len(messages)-1):
        delta = messages[i+1].time - messages[i].time
        if delta.total_seconds() < 60:
            if sequence:
                sequence.append(messages[i+1])
            else:
                sequence = [messages[i], messages[i+1]]
        else:
            if sequence:
                if len(sequence) >= 3:
                    sequence.append(messages[i+1])
                    pp.pprint(sequence)
                    print("")
                sequence = None

    print("\n-----------------------\Fast OUT->IN = ")
    for i in range(len(messages)-1):
        m1 = messages[i]
        m2 = messages[i+1]
        if (m2.state == 1) and (m1.state == 0):
            delta = m2.time - m1.time
            if delta.total_seconds() < 25:
                print("{} {} >> {} {}".format(m1.value, m1.time, m2.value,m2.time))

    print("\n-----------------------\Fast IN->OUT = ")
    for i in range(len(messages)-1):
        m1 = messages[i]
        m2 = messages[i+1]
        if (m2.state == 0) and (m1.state == 1):
            delta = m2.time - m1.time
            if delta.total_seconds() < 25:
                print("{} {} >> {} {}".format(m1.value, m1.time, m2.value,m2.time))
 
    
