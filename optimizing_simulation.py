


def good_enough(result):
    return result >= 0.999
    
def bad_enough(result):
    return result <= 0.1
    
def interesting_result(result):
    return (not good_enough(result)) and (not bad_enough(result))

settings = [ 0,1,2,3,4,5,6,7]
#results = { 0:1,1:1,2:0.7,3:0.6,4:0.1,5:0,6:0,7:0 }
#results = { 0:0.5,1:0.5,2:0.5,3:0.5,4:0.5,5:0.5,6:0.5,7:0.5}
results = { 0:1,1:1,2:0.5,3:0,4:0,5:0,6:0,7:0}
def compute(setting):
    res = results[setting]
    print("{} {}".format(setting,res))
    return res


def optimized_simulation():
    limits = (0 , len(settings))
    while limits[1] - limits[0]:
        index = (limits[1] - limits[0])//2
        res = compute(settings[index])
        if interesting_result(res):
            for i in range(index+1, limits[1]):
                res = compute(settings[i])
                if bad_enough(res):
                    print("bad enough")
                    break
            for i in range(index-1, -1, -1):
                res = compute(settings[i])
                if good_enough(res):
                    print("good enough")
                    break       
            return
        if bad_enough(res):
            limits = (0 , index)
            continue
        if good_enough()(res):
            limits = (index+1 , settings)
            continue
    
if __name__ == '__main__':
    optimized_simulation()