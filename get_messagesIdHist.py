import re
import operator
import pprint

class Message:
    def __init__(self, value, modem, base_station, power):
        self.value = value
        self.modem = modem
        self.base_station = base_station
        self.power = power
    def __str__(self):
        return "{0} {1}".format(self.value, self.modem)
    def __repr__(self):
        return str(self)
messages = []
if __name__ == "__main__":
    f = open('data.txt', 'r')

    line_counter = 0
    lines = [None, None, None]
    for line in f:
        #print (line)
        lines[line_counter] = line.split()
        line_counter = line_counter + 1
        if line_counter == 3:
            line_counter = 0
            messages.append(
                Message(lines[1][0], lines[0][1], lines[0][0], None))
            full_line = '';
    f.close()
    print ("Removing duplicates")
    find_duplicates = True
    while find_duplicates:
        find_duplicates = False
        for i in range(len(messages) - 1):
            if messages[i].value == messages[i+1].value and messages[i].modem == messages[i+1].modem:
                print(messages[i])
                del messages[i]
                find_duplicates = True
                break


    messages.reverse()
    print ("Received messages num = {0}".format(len(messages)))

    ids_count = {}        
    
    for mes in messages:
        if mes.modem in ids_count:
            ids_count[mes.modem] += 1
        else:
            ids_count[mes.modem] = 1

    ##print("ids_count = ", ids_count)
    pp = pprint.PrettyPrinter(indent=4)
    sorted_ids_count = list(reversed(sorted(ids_count.items(), key=operator.itemgetter(1))))
    print("sorted_ids_count = ")
    pp.pprint(sorted_ids_count)
