import os
import operator



BIG_SIZE = 100 * 1024 * 1024
big_files = {}
print("Big files:")
for root, dirs, files in os.walk(".", topdown=False):
    for name in files:
        fullname = os.path.join(root, name)
        try:
            size = os.path.getsize(fullname)
            if size >= BIG_SIZE:
                big_files[fullname] = size
        except Exception as e:
            pass

file_list = reversed(sorted(big_files.items(), key=operator.itemgetter(1)))
for file in file_list:
    print("{} - {}MB".format(file[0], round(file[1]/(1024*1024), 1)))
input("Enter any key to exit")
