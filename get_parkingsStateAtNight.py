#! python3
import re
import operator
import pprint
import datetime
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

class Message:
    def __init__(self, value, modem, base_station, power, date_time):
        self.value = value
        self.modem = modem
        self.base_station = base_station
        self.power = power
        self.date_time = date_time
        self.time = datetime.datetime.strptime(date_time, "%y-%m-%d %H:%M:%S")
        self.state = 0 if self.value[3] == '0' else 1
    def __str__(self):
        return "{0} {1} {2}".format(self.value, self.modem, self.time)
    def __repr__(self):
        return str(self)

if __name__ == "__main__":
    messages = []
    pp = pprint.PrettyPrinter(indent=4)
    f = open('data.txt', 'r')

    line_counter = 0
    lines = [None, None, None]
    for line in f:
        #print (line)
        lines[line_counter] = line.split()
        line_counter = line_counter + 1
        if line_counter == 3:
            line_counter = 0
            messages.append(
                Message(lines[1][0], lines[0][1], lines[0][0], None, lines[2][1] + " " + lines[2][2]))
            full_line = '';
    f.close()
    print ("Removing duplicates")
    find_duplicates = True
    while find_duplicates:
        find_duplicates = False
        for i in range(len(messages) - 1):
            if messages[i].value == messages[i+1].value and messages[i].modem == messages[i+1].modem:
                print(messages[i])
                del messages[i]
                find_duplicates = True
                break


    messages.reverse()
    debug_messages = list(filter(lambda x: x.value[2:4] == "DD", messages))
    messages = list(filter(lambda x: x.value[2:4] != "DD", messages))
    
    print ("Received messages num = {0}".format(len(messages)))

    

    print("debug_messages = ")
    pp.pprint(debug_messages)

    print("messages = ")
    pp.pprint(messages)

    dates = []
    values = []

    state = 0
    for m in messages:
        dates.append(m.time - datetime.timedelta(0, 1))
        values.append(state)
        state = 0 if m.value[3] == '0' else 1
        dates.append(m.time)
        values.append(state)

    dates = matplotlib.dates.date2num(dates)
    plt.plot_date(dates, values,'b-')

    t = datetime.datetime(messages[0].time.year, messages[0].time.month, messages[0].time.day)


    last = datetime.datetime(messages[-1].time.year, messages[-1].time.month, messages[-1].time.day)+datetime.timedelta(1, 0)

    dates = []
    while t < last:
        dates.append(t)
        t = t + datetime.timedelta(0, 3600)  # days, seconds, then other fields.

    values = [ -0.2 if d.hour < 5 or d.hour>23 else 1.2 for d in dates]
    dates = matplotlib.dates.date2num(dates)
    plt.plot_date(dates, values, 'r-')

    axes = plt.gca()
    axes.set_ylim([-0.5, 1.5])
    plt.show()


